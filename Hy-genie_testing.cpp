#include <iostream>
#include "hal_RFID.h"

/*
  The time near a specific tag may work,
  need to look into how reading the RFID 
  multiple times would work.
  May cause massive power drain.
*/

/*
  RFID data will need to be collected to be checked
  will not need to store the data if it dose not 
  have the check byte as part of it, need to find out 
  if this is possible.
*/

/*
  Need to find if either notify or an actual connection
  would be better suited for this solution. 
*/

/*
  The central bluetooth section of this solution still 
  needs to be either considered or planed out as currently 
  nothing exists or has any consept built for it.
*/

/*
  Need to know if the solution will be able to know it's
  battery level primarily for development but also 
  as part of the final product.
*/

/*
  This section of the code should be used to set up all
  of the pins for the hardware communication. The serial
  set up will be done later on in the program depending on
  when different things are needed.
*/

int main()
{
  RFID RFID_Run_Read;

  std::cout << "RFID Read: " << RFID_Run_Read.RFID_Read(true);

  return 0;
}

/*
class RFID {
  //Will make sense to make an RFID class

  //The below method will check for an RFID tag in range 
  //and will the asses if it should be stored.
  int RFID_Read()
  {
    //If RFID Signal is found
    //RFID_Proximity
    //else
    //Stand_by
    return 0;
  }

  //The below method will be used to check the perceived
  //distance from the device to the tag using the signal
  //strength, this will be dependent upon the distance 
  //as well as any obsticals in the way.
  int RFID_Proximity()
  {
    //If RFID Proximity < max_distance
    //RFID_Form_Persistance_Check
    //else
    //Stand_by
    return 0;
  }

  //The below method will be used to check to see if the
  //RFID tag found is one that the system wants to collect.
  //This method will also check that the found tag can 
  //be found for a number of seconds with a tick loop
  //(or similar)
  int RFID_Form_Persistance_Check()
  {
    //for i in range(0, 5)
    //delay() //TO Be decided
    //if RFID_read == first
    //persistanceInt ++
    //end loop
    //if persistanceInt > 3 && persistanceInt < 7
    //RFID String
    return 0;
  }

  //The below method will be used to take the RFID data 
  //and use it as a string thoughout the program.
  void RFID_String()
  {
    char RFID[] = "";
    //If string build succsess
    //Data_buffer_Setup
    return;
  }
};
*/

class BLE{
  //Will make sense to make a BLE class

  //The below method will be used to connect from the device
  //to the desired bluetooth device, this will allow for the 
  //connection to be found and tested before any data is sent.
  int BLE_Connect() 
  {
    //If connection success 
    //BLE_Data_Send
    //else
    //Stand_by
    return 0;
  }

  //The below method will be used to send the data to the desired
  //BLE device, it would make sense for this to clear the buffer
  //as the data is sent.
  int BLE_Data_Send()
  {
    //If not critical
    //For all in Data_Buffer
    //If receive conformation
    //Data_Buffer_Clear(Current position)
    //else
    //Stand_By
    //If critical
    //send battery sos
    //If received
    //retrun true
    //else 
    //return false
    return 0;
  }
};

class Data {
  //It would make sense for the data manipulation section of the program
  //to have its own class

  //The below method will be used to build and maintin the data 
  //buffer fro the running of the program, this may need to change
  //depending on hardware limitations.
  int Data_Buffer_Setup()
  {
    //If buffer already built
    //Data_Buffer_Add
    //else set up a new data buffer
    //If Wake
    //BLE_Connection
    return 0;
  }

  //The below method will be used to add new data to the data buffer
  //this may not be needed as the Data_Buffer_Setup() can have this
  //functionality added into it as opposed to need a whole method but
  //this will make sure that this functionality is not forgoten.
  int Data_Buffer_Add()
  {
    //If Data added success
    //If Wake
    //BLE_Connection
    return 0;
  }

  //The below method will be used to clear the data buffer after it has
  //been sent via the BLE_Data_Send method. This method may need to only
  //clear from positions in the queue so that data is only deleted from
  //the device after it has been sent.
  int Data_Buffer_Clear()
  {
    //If Data_Buffer remove(current) && Data_Buffer still populated
    //return
    //else if Data_Buffer empty
    //Stand_By
    return 0;
  }
};

//The below class will hold methods that will be used to cause specific 
//interactions between the different parts of the hardware while having
//little to do with the overall data manipulation.
class Hardware_Specific{
public:
  //The below method will be used to set up the communication between
  //the different hardware aspects of the system, the ports will be 
  //defined at the top of the program when they are known.
  int Serial_Setup()
  {
    //If sent == RFID
    //set up RFID Connection
    //else if sent == BLE
    //set up BLE connection
    //else
    //end BLE connection
    //end RFID connection
    return 0;
  }

  //The below method will set up the first of different power 
  //levels to ensure that the system can operate as long as
  //possible. This will be the highest level with all of the
  //components able to operate as needed.
  int Wake()
  {
    //Serial_Setup(Device)
    //If succeed 
    //return true
    return 0;
  }

  //The below method will be the second power state with only
  //the RFID and timing being powered, the RFID will be able
  //to have it's power state changed on a timing basis so it
  //is not on all the time but will still work as an interrupt.
  int Stand_By()
  {
    //if Battery_Level > 20
    //Read_RFID
    //else
    //Critical
    return 0;
  }

  //The below method will be the third and final power state
  //this will run when the battery is getting close to a 
  //critical level and will set the RFID to its off state as 
  //well as, if it has not already been received, send a 
  //distress signal to the BLE terminal so that the user 
  //is able to know that the device is close to death
  int Critical()
  {
    //while (sos_Sent == false)
    //If BLE_Connection
    //If BLE_Send(Critical)
    //Serial_Setup(End)
    //Low power mode
    //set sos_sent = true
    //else
    //low power mode waiting for tick
    return 0;
  }

  //The below method will be used to check the power level
  //of the battery so that the apropiate sleep state can be 
  //applied to the system, this will also be part of the 
  //data that is sent to the central system so that users
  //can keep an eye on the power level of all of the 
  //devices.
  int Battery_Level()
  {
    //If battery level check success
    //return battery level
    //else 
    //return error val
    return 0;
  }
};

